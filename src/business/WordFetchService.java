/*
 * Creator: Donald Trowbridge
 * Date: 12/13/2020
 */

package business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import beans.Word;

/**
 * Session Bean implementation class WordFetchService
 */
@Stateless
@LocalBean
public class WordFetchService {
	
	private String BibleApiUrl = "https://api.scripture.api.bible/v1/bibles/de4e12af7f28f599-01";
	private String ApiKey = "f40e107ed7abb6a80052c68425b6bf7d";
    /**
     * Default constructor. 
     */
    public WordFetchService() {
        // TODO Auto-generated constructor stub
    }

    public void findOccurences(Word word) {
    	try {
    		/*
    		 * Create URL and open connection to Bible API
    		 */
    		String urlStr = BibleApiUrl + "/search?query=" + word.getWord() + "&limit=1";
    		URL url = new URL(urlStr);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty("api-key", ApiKey);
    		
    		System.out.println("Fetching word count for "+ word.getWord() + "\nConnection status ==> " + conn.getResponseCode());
    		if(conn.getResponseCode() == 200) {
    			/*
    			 * Read in the input stream from the Bible API and close the connection
    			 */
    			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String contents = bufferReader.readLine();
				bufferReader.close();
				conn.disconnect();
				
				/*
				 * Parse the total value and set the occurences attribute of word to value
				 */
				String totalPattern = "\"total\":";
				int start = contents.indexOf(totalPattern);
				if(start > 0) {
					start += totalPattern.length();
					String total = contents.substring(start, contents.indexOf(",", start));
					try {
						int totalInt = Integer.parseInt(total);
						word.setOccurences(totalInt);
					} catch(Exception e) {
						word.setOccurences(-1);
						e.printStackTrace();
					}
				}
    		}
    	}catch(Exception e) {
    		word.setOccurences(-1);
    		e.printStackTrace();
    	}
    }
}
